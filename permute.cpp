#include <iostream>
#include <string>
#include <vector>
#include "perm.h"
using namespace std;


int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		std::cout << "Proper input is 'permute string [search_strings] [> output_file]'" << std::endl;
		return 1;
	}
	else
	{
		std::string input(argv[1]);


		if (argc == 2)
		{
			permute_cout(input);
		}
		else
		{
			// first arg is input string			
			std::vector<std::string> search_strings(argc-2);
			for (int j = 2; j < argc; j++)
			{
				std::string str(argv[j]);
				if (str.compare(">") == 0)
				{
					// redirected output is not part of mathcing strings
					// resizing vector length
					search_strings.resize(argc - j);
					break;
				}
				else
				{								
					search_strings[j - 2] = str;
				}

			}


			permute_and_search_cout(input, search_strings);


		}
		
	}

	return 0;
}
