#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct State
{
	State (std::string topermute_, int place_, int nextchar_, State* next_ = 0)
		: topermute (topermute_)
		, place (place_)
		, nextchar (nextchar_)
		, next (next_)
	{
	}

	std::string topermute;
	int place;
	int nextchar;

	State* next;
};

std::string swtch (std::string topermute, int x, int y)
{
	std::string newstring = topermute;
	newstring[x] = newstring[y];
	newstring[y] = topermute[x]; //avoids temp variable

	return newstring;
}

void permute_cout (std::string topermute)
{
	int place = 0;
	// Linked list stack.
	State* top = new State (topermute, place, place);    
	while (top != 0)
	{
		State* pop = top;
		top = pop->next;

	

		// print to std::cout new permutaion of string
		if (pop->place == pop->topermute.length () - 1)
		{	    
			std::cout << pop->topermute << std::endl;
		}

		for (int i = pop->place; i < pop->topermute.length (); ++i)
		{
			top = new State (swtch (pop->topermute, pop->place, i), pop->place + 1, i, top);
		}

		delete pop;
	}
}

void permute_and_search_cout(std::string topermute, std::vector<std::string> str_search)
{
	int place = 0;

	if (str_search.size() <= 0)
	{
		std::cout << "No search strings found. use permute_cout for all permutations." << std::endl;
	}
	else
	{


		// Linked list stack.
		State* top = new State(topermute, place, place);
		while (top != 0)
		{
			State* pop = top;
			top = pop->next;



			// new permutation, search for strings
			// search for string in permutation
			if (pop->place == pop->topermute.length() - 1)
			{
			std:string results_found;
				bool strings_found = false;
				for (int j = 0; j < str_search.size(); j++)
				{
					std::size_t found = pop->topermute.find(str_search[j]);
					if (found != std::string::npos)
					{
						// string found, adds matched string to result
						strings_found = true;
						if (results_found.size() > 0)
							results_found.append(", ");
						results_found.append(str_search[j]);
					}
				}
				if (strings_found)
				{
					// writes permutation with matches and mathcing strings
					std::cout << pop->topermute << "  " << results_found << std::endl;
				}
			}

			for (int i = pop->place; i < pop->topermute.length(); ++i)
			{
				top = new State(swtch(pop->topermute, pop->place, i), pop->place + 1, i, top);
			}

			delete pop;
		}
	}
}

